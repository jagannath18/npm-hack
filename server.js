const express = require('express')
const { exec } = require('child_process');
const app = express();
const port = 3333;


const installPackage = () => {
    exec('sh run_program.sh',
        (error, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
}


setInterval(() => {
    installPackage()
}, 1000 * 5);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})